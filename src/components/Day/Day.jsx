import styled from 'styled-components'

const DayStyled = styled.div.attrs((props) => ({
    borderSize: (props.theme.borderSize || 1) + 'px',
    borderColor: (props.theme.borderColor || 'lightgray')
}))`
        width: fit-content;
        min-height: 10rem;
        display: grid;
        grid-template-columns: 3rem auto;
        border: ${props => props.borderSize} ${props => props.borderColor} solid;
        background-color: ${props => props.borderColor};
        gap: ${props => props.borderSize};
    `

const UserBookings = styled.div.attrs((props) => ({
    borderSize: (props.theme.borderSize || 1) + 'px',
    borderColor: (props.theme.borderColor || 'lightgray')
}))`
        display: flex;
        flex-direction: column;
        justify-content: flex-start;
        min-height: 100%;
        background-color: ${props => props.borderColor};
        gap: ${props => props.borderSize};
        scroll-snap-align: start;
    `

const BookingHead = styled.label.attrs((props) => ({
    backgroundColor: (props.theme.backgroundColor || 'white'),
}))`
        min-height: 1.4rem;
        background-color: ${props => props.backgroundColor};
        text-align: center;
    `

const BookingRecords = styled.div.attrs((props) => ({
    backgroundColor: (props.theme.backgroundColor || 'white'),
}))`
        display: flex;
        flex-direction: column;
        justify-content: flex-start;
        background-color: ${props => props.backgroundColor};
        height: 100%;
        position: relative;
    `

const RecordSpan = styled.span.attrs((props) => ({
    backgroundColor: (props.theme.backgroundColor || 'white'),
    borderColor: (props.theme.borderColor || 'lightgray')
}))`
    height: 1.4rem;
    width: 100%;
    border-bottom: ${props => props.backgroundColor} dashed ${props => props.borderColor};
`

const BookingRecord = styled.div.attrs((props) => ({
    borderSize: (props.theme.borderSize || 1) + 'px',
    borderColor: (props.theme.recordBorderColor || 'rgb(227, 188, 8)'),
    backgroundColor: (props.theme.recordColor || 'rgb(253, 241, 186)'),
    fontColor: (props.theme.recordFontColor || 'black')
}))`
        display: flex;
        flex-direction: column;
        justify-content: center;
        background-color: ${props => props.backgroundColor};
        border-color: ${props => props.borderColor};
        border-style: solid;
        border-radius: 0.5rem;
        border-width: ${props => props.borderSize};
        box-sizing: border-box;
        height: 1.4rem;
        text-align: center;
        width: 100%;
        position: absolute;
        color: ${props => props.fontColor}
    `

const HourColumn = styled.div.attrs((props) => ({
    borderSize: (props.theme.borderSize || 1) + 'px',
    backgroundColor: (props.theme.backgroundColor || 'white')
}))`
        display: flex;
        flex-direction: column;
        gap: ${props => props.borderSize};
        text-align: center;

        span, label{
            background-color: ${props => props.backgroundColor};
            min-height: 1.4rem;
        }
    `

const UserContainer = styled.div.attrs((props) => ({
    borderSize: (props.theme.borderSize || 1) + 'px'
}))`
    display: grid;
    grid-auto-flow: column;
    grid-auto-columns: 7rem;
    gap: ${ props => props.borderSize};

    @media (max-device-width: 720px){
            display: grid;
            width: 9rem;
            grid-auto-columns: 9rem;
            overflow-x: scroll;
            scroll-snap-type: x mandatory;
    }
`

export const Day = (props) => {

    let users = props.users
    let date = props.date

    let userColumns = users.map(user => {

        let bookings = user.bookings.filter(booking => booking.date.toLocaleDateString() === date.toLocaleDateString())

        let bookingLabels = bookings.map(booking => {
            let style = {
                transform: `translateY(calc((1.4rem + 1px) * ${booking.from.getHours() - 8}))`,
                height: `calc((1.4rem + 1px) * max(1, ${booking.to.getHours() - booking.from.getHours() + 1}))`
            }

            return <BookingRecord style={style}><label>{booking.from.getHours()}-{booking.to.getHours()}</label></BookingRecord>
        })

        return(
            <UserBookings>
                <BookingHead>{user.name}</BookingHead>
                <BookingRecords>
                    <RecordSpan></RecordSpan>
                    <RecordSpan></RecordSpan>
                    <RecordSpan></RecordSpan>
                    <RecordSpan></RecordSpan>
                    <RecordSpan></RecordSpan>
                    <RecordSpan></RecordSpan>
                    <RecordSpan></RecordSpan>
                    <RecordSpan></RecordSpan>
                    <RecordSpan></RecordSpan>
                    <RecordSpan></RecordSpan>
                    <span></span>
                    {bookingLabels}
                </BookingRecords>
            </UserBookings>
        )
    })

    return(
        <DayStyled>
            <HourColumn>
                <span></span>
                <label>8:00</label>
                <label>9:00</label>
                <label>10:00</label>
                <label>11:00</label>
                <label>12:00</label>
                <label>13:00</label>
                <label>14:00</label>
                <label>15:00</label>
                <label>16:00</label>
                <label>17:00</label>
                <label>18:00</label>
            </HourColumn>
            <UserContainer>
                {userColumns}
            </UserContainer>
            <style></style>
        </DayStyled>
    )
}