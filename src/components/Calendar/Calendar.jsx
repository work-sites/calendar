import { useState } from 'react'
import { Month } from '../Month/Month'
import { Week } from '../Week/Week'
import { Day } from '../Day/Day'
import styled, { ThemeProvider } from 'styled-components'

const CalendarStyled = styled.div.attrs((props) => ({
    fontSize: (props.theme.fontSize || 1) + "em",
    fontFamily: (props.theme.fontFamily || "sistem-ui, sans-serif"),
    color: (props.theme.color || 'black')
}))`
    font: ${props => props.fontSize} ${props => props.fontFamily};
    display: flex;
    flex-direction: column;
    align-items: center;
    gap: 3em;
    color: ${props => props.color};
    max-width: 100vw;
    width: 90vw;
    box-sizing: border-box;
    @media (max-device-width: 720px){
    }
`

const Panel = styled.div`
    display: grid;
    grid-template-columns: 1fr auto 1fr;
    gap: 1rem;
    text-align: center;
    align-items: center;

    @media (max-device-width: 720px){
        grid-template-columns: 1fr;
        grid-template-rows: 1fr 1fr 1fr;
        justify-items: center;
    }
`

const MenuButton = styled.button.attrs((props) => ({
    backgroundColor: (props.theme.panelColor || '#0d6efd'),
    fontColor: (props.theme.panelFontColor || 'white'),
    hoverColor: (props.theme.panelHoverColor || '#0b5ed7')
}))`
    border: unset;
    border-radius: 0;
    cursor: pointer;
    padding: 0.3rem;
    background-color: ${props => props.$selected ? props.hoverColor : props.backgroundColor};
    color: ${props => props.fontColor};
    transition: all 0.3s;

    &:hover{
        background-color: ${props => props.hoverColor}; 
    }
`

const TodayButton = styled(MenuButton).attrs((props) => ({
    backgroundColor: (props.theme.todayColor || 'white'),
    fontColor: (props.theme.todayFontColor || '#6c757d')
}))`
    background-color: ${props => props.backgroundColor};
    color: ${props => props.fontColor};
    border-block: 1px solid ${props => props.fontColor};

    &:hover{
        background-color: ${props => props.fontColor}; 
        color: ${props => props.backgroundColor};
    }
`

const FirstMenuButton = styled(MenuButton)`
        border-bottom-left-radius: 0.2rem;
        border-top-left-radius: 0.2rem;
    `

const LastMenuButton = styled(MenuButton)`
        border-bottom-right-radius: 0.2rem;
        border-top-right-radius: 0.2rem;
    `

const Menu = styled.div`
        display: flex;
        flex-direction: row;
    `

const ComponentContainer = styled.div`
    width: 100%;
    display: flex;
    flex-direction: row;
    justify-content: center;

    @media (max-device-width: 720px){
        width: min-content;
    }
`

const BackButton = styled(MenuButton)`
    margin-right: 1rem;
    border-radius: 0.2rem;
`

export const Calendar = (props) => {

    let [date, setDate] = useState(new Date())
    let [currentOption, setCurrentOption] = useState("month")
    let [actions, setActions] = useState([])

    let testProps = [{
        name: 'Руслан', 
        bookings: [
        {
            date: new Date(),
            from: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate(), 12),
            to: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate(), 14)
        },
        {
            date: new Date("December 12 2023"),
            from: new Date(new Date("December 12 2023").getFullYear(), new Date("December 12 2023").getMonth(), new Date("December 12 2023").getDate(), 12),
            to: new Date(new Date("December 12 2023").getFullYear(), new Date("December 12 2023").getMonth(), new Date("December 12 2023").getDate(), 14)
        }
    ]
    },
    {
        name: 'Дмитрий',
        bookings: [
            {
                date: new Date(),
                from: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate(), 12),
                to: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate(), 14)
            },
            {
                date: new Date("December 12 2023"),
                from: new Date(new Date("December 12 2023").getFullYear(), new Date("December 12 2023").getMonth(), new Date("December 12 2023").getDate(), 12),
                to: new Date(new Date("December 12 2023").getFullYear(), new Date("December 12 2023").getMonth(), new Date("December 12 2023").getDate(), 14)
            }
        ]
    }]

    

    const changeDatePlus = () => {
        let newDate = new Date(date.valueOf())

        switch(currentOption){
            case "month": newDate.setMonth(newDate.getMonth() + 1, 1) 
                break
            case "week": newDate.setDate(newDate.getDate() + 7)
                break
            case "day": newDate.setDate(newDate.getDate() + 1)
                break
            default: console.log("sommething went wrong")
                break
        }

        setDate(newDate)
    }

    const changeDateMinus = () => {
        let newDate = new Date(date.valueOf())

        switch (currentOption) {
            case "month": newDate.setMonth(newDate.getMonth() - 1, 1) 
                break
            case "week": newDate.setDate(newDate.getDate() - 7)
                break
            case "day": newDate.setDate(newDate.getDate() - 1)
                break
            default: console.log("sommething went wrong")
                break
        }

        setDate(newDate)
    }

    const toDay = (newDate) => {
        let newActions = [...actions]
        newActions.push({ date: date, option: currentOption })
        setActions(newActions)
        setDate(newDate)
        setCurrentOption("day")
    }

    const handleAction = (option) => {
        if(currentOption === option){ return}
        let newActions = [...actions]
        newActions.push({date: date, option: currentOption})
        setActions(newActions)
        setCurrentOption(option)
    }

    const handleDateChange = (dateChangeFunction) => {
        let newActions = [...actions]
        newActions.push({ date: date, option: currentOption })
        setActions(newActions)
        dateChangeFunction()
    }

    const handleBack = () => {
        let newActions = [...actions]
        let action = newActions.pop()

        if(!action){return}

        setCurrentOption(action.option)
        setDate(action.date)
        setActions(newActions)
    }

    return(
        <ThemeProvider theme={props.theme}>
        
        <CalendarStyled>
            <Panel>
                <Menu>
                    <BackButton onClick={handleBack}>Back</BackButton>
                    <FirstMenuButton onClick={() => handleDateChange(changeDateMinus)} className='first'>Previous</FirstMenuButton>
                    <TodayButton onClick={() => handleDateChange(() => setDate(new Date()))}>Today</TodayButton>
                    <LastMenuButton onClick={() => handleDateChange(changeDatePlus)} className='last'>Next</LastMenuButton>
                </Menu>
                <label>{date.toLocaleDateString("en-US", {year: "numeric", month: "long", day: "numeric"})}</label>
                <Menu>
                    <FirstMenuButton onClick={() => handleAction("month")} $selected={currentOption === "month"}>Month</FirstMenuButton>
                    <MenuButton onClick={() => handleAction("week")} $selected={currentOption === "week"}>Week</MenuButton>
                    <LastMenuButton onClick={() => handleAction("day")} $selected={currentOption === "day"}>Day</LastMenuButton>
                </Menu>
            </Panel>
            <ComponentContainer>
                {currentOption === "month" ? <Month date={date} users={testProps} changeFunction={toDay}/> : null}
                {currentOption === "week" ? <Week date={date} users={testProps} changeFunction={toDay}/> : null}
                {currentOption === "day" ? <Day date={date} users={testProps}/> : null}
            </ComponentContainer>
        </CalendarStyled>

        </ThemeProvider>
    )
}