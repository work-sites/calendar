import styled from "styled-components"
import { DayOfWeek } from "../DayOfWeek/DayOfWeek"

const WeekStyled = styled.div.attrs((props) => ({
    borderSize: (props.theme.borderSize || 1) + 'px',
    borderColor: (props.theme.borderColor || 'lightgray'),
    backgroundColor: (props.theme.backgroundColor || 'white'),
}))`
        width: fit-content;
        display: grid;
        grid-template-columns: 0.5fr auto;
        background-color: ${props => props.borderColor};
        gap: ${props => props.borderSize};
        border: ${props => props.borderSize} solid ${props => props.borderColor};
        text-align: center;

        span, label {
            min-height: 1.4rem;
            background-color: ${props => props.backgroundColor};
            padding: 0 1rem;
        }
    `

const HourColumn = styled.div.attrs((props) => ({
    borderSize: (props.theme.borderSize || 1) + 'px'
}))`
        display: flex;
        flex-direction: column;
        gap: ${props => props.borderSize};
        font-weight: 600;
    `

const WeekContaner = styled.div.attrs((props) => ({
    borderSize: (props.theme.borderSize || 1) + 'px',
}))`
    display: grid;
    grid-template-columns: repeat(7, 1fr);
    gap: ${props => props.borderSize};

    @media (max-device-width: 720px){
        width: 9rem;
        grid-template-columns: repeat(7, 9rem);
        overflow-x: scroll;
        scroll-snap-type: x mandatory;
    }
`

export const Week = (props) => {

    let week_offset = props.date.getDay()
    if(week_offset === 0){week_offset = 7}

    let start_date = new Date(props.date.getFullYear(), props.date.getMonth(), props.date.getDate() - week_offset)

    let week_days = []
    for(let i = 1; i < 8; i++){
        let week_date = new Date(start_date.getFullYear(), start_date.getMonth(), start_date.getDate() + i)
        let week_day = <DayOfWeek date={week_date} changeFunction={props.changeFunction}/>
        week_days.push(week_day)
    }

    return (
        <WeekStyled>
            <HourColumn>
                <span></span>
                <label>8:00</label>
                <label>9:00</label>
                <label>10:00</label>
                <label>11:00</label>
                <label>12:00</label>
                <label>13:00</label>
                <label>14:00</label>
                <label>15:00</label>
                <label>16:00</label>
                <label>17:00</label>
                <label>18:00</label>
            </HourColumn>
            <WeekContaner>
                {week_days}
            </WeekContaner>
        </WeekStyled>
    )
}