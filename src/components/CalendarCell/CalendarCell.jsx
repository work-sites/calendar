import styled from 'styled-components'

const CalendarCellStyled = styled.div.attrs((props) => ({
    backgroundColor: (props.theme.backgroundColor || 'white'),
    color: (props.theme.calendarColor || props.theme.color || "gray")
}))`
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
    min-height: 4rem;
    background-color: ${props => props.backgroundColor};
    cursor: pointer;
    padding: 0.5rem;
    box-sizing: border-box;
    color: ${props => props.color};

    &:hover{
        filter: brightness(90%);
    }
`

export const CalendarCell = (props) => {

    const handleClick = () => {
        let cell_date = new Date(props.date.getFullYear(), props.date.getMonth(), props.day)
        props.changeFunction(cell_date)
    }

    return(
        <CalendarCellStyled onClick={handleClick}>
            <label>{props.day}</label>
        </CalendarCellStyled>
    )
}