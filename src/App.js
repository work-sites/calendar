import { Calendar } from './components/Calendar/Calendar';

function App() {
  return (
    <Calendar theme={{ fontSize: 1}}/>
  );
}

export default App;
