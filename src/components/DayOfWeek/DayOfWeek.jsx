import styled from "styled-components";

const HourColumn = styled.div.attrs((props) => ({
    borderSize: (props.theme.borderSize || 1) + 'px',
    borderColor: (props.theme.borderColor || 'lightgray'),
    backgroundColor: (props.theme.backgroundColor || 'white'),
}))`
        display: flex;
        flex-direction: column;
        gap: ${props => props.borderSize};
        scroll-snap-align: start;

        *{
           background-color: ${props => props.backgroundColor}; 
        }
        
        &:hover *{
            cursor: pointer;
            filter: brightness(98%);
        }

        & label:hover{
            filter: brightness(90%);
        }
    `

export const DayOfWeek = (props) => {

    let WEEK_DAYS = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]

    let day = props.date.getDay()
    if(day === 0){day = 7}
    let day_label = WEEK_DAYS[day - 1]

    let hours = []
    for(let i = 0; i < 11; i++){
        let hour = <label></label>
        hours.push(hour)
    }

    return(
        <HourColumn onClick={() => props.changeFunction(props.date)}>
            <label>{day_label}</label>
            {hours}
        </HourColumn>
    )
}