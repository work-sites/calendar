import styled from 'styled-components';
import { CalendarCell } from '../CalendarCell/CalendarCell';

const MonthStyled = styled.div.attrs((props) => ({
    borderSize: (props.theme.borderSize || 1) + 'px',
    borderColor: (props.theme.borderColor || 'lightgray')
}))`
        display: grid;
        grid-template-columns: repeat(7, 1fr);
        border: ${props => props.borderSize} ${props => props.borderColor} solid;
        gap: ${props => props.borderSize};
        background-color: ${props => props.borderColor};

        label{
            text-align: center;
        }
    `

const WeekLabel = styled.label.attrs((props) => ({
    backgroundColor: (props.theme.backgroundColor || 'white')
}))`
    background-color: ${props => props.backgroundColor};
    font-weight: 700;
    padding: 0 1rem;
`

const FillSpan = styled.span.attrs((props) => ({
    backgroundColor: (props.theme.backgroundColor || 'white')
}))`
    background-color: ${props => props.backgroundColor};
    filter: brightness(95%);
`

export const Month = (props) => {

    let date = props.date
    let days = new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate()

    let dayGrid = []

    let offset = new Date(date.getFullYear(), date.getMonth(), 1).getDay()
    if (offset === 0) { offset = 7 }
    for(let i = 0; i < offset - 1; i++){
        dayGrid.push(<FillSpan></FillSpan>)
    }

    console.log(props.changeFunction)

    for(let i = 0; i < days; i++){
        let cell = <CalendarCell day={i+1} date={date} changeFunction={props.changeFunction}/>
        dayGrid.push(cell)
    }

    let endOffset = new Date(date.getFullYear(), date.getMonth() + 1, 0).getDay()
    if(endOffset === 0){endOffset = 7}
    for (let i = 0; i < 7 - endOffset; i++) {
        dayGrid.push(<FillSpan></FillSpan>)
    }


    return (
        <MonthStyled>
            <WeekLabel>Monday</WeekLabel>
            <WeekLabel>Tuesday</WeekLabel>
            <WeekLabel>Wednesday</WeekLabel>
            <WeekLabel>Thursday</WeekLabel>
            <WeekLabel>Friday</WeekLabel>
            <WeekLabel>Saturday</WeekLabel>
            <WeekLabel>Sunday</WeekLabel>
           {dayGrid} 
           <style></style>
        </MonthStyled>
    )
}